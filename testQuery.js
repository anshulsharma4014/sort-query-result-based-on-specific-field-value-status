    /**
    * Add compare method to sort data from backend based on specific field from backend
    */
    compare(a, b) {
        if (a.sortOrder < b.sortOrder) {
            return -1;
        }
        if (a.sortOrder > b.sortOrder) {
            return 1;
        }
        return 0;
    }

    getApplications() {
        this.showSpinner = true;
        getAmendmentApplications()
            .then(resp => {
                this.applications = resp;
                this.hasAnyApplication = this.applications.length > 0;
                
                if( this.hasAnyApplication ) {
                    this.applications.sort(this.compare);
                }
            }).catch(err => {
                console.log(err);
            }).finally(() => {
                this.showSpinner = false;
            })
    }