/**
 * @description       : 
 * @author            : Anshul Sharma
 * @group             : 
 * @last modified on  : 05-30-2021
 * @last modified by  : Anshul Sharma
 * Modifications Log 
 * Ver   Date         Author          Modification
 * 1.0   05-30-2021   Anshul Sharma   Initial Version
**/
public with sharing class TH_SortResults {

    /**
    * These variables are used to give sort order to applications based on their status.
    * Assuming more than 10000 applications will not be returned at one time, with same status
    */
    private static Integer sortOrderActive = 10000;
    private static Integer sortOrderRenewed = 20000;
    private static Integer sortOrderNew = 40000;
    private static Integer sortOrderDraft = 50000;
    private static Integer sortOrderInProgress = 60000;

    @AuraEnabled
    public static List<ApplicationWrapper> getApplications(){
        try {
            List<ApplicationWrapper> wrapper = new List<ApplicationWrapper>();
            List<String> appStatusList = new List<String> {TH_Constants.ACTIVE, TH_Constants.RENEWED, TH_Constants.NEW, TH_Constants.DRAFT, TH_Constants.IN_PROGRESS};
            User currentUser = [SELECT Id, ContactId, CreatedDate, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId()];

            List<Application__c> applicationList = [SELECT Id, Application_Status__c, Name, Effective_Date__c 
                                                    FROM Application__c
                                                    WHERE Contact_Id__c = :currentUser.ContactId AND Application_Status__c IN :appStatusList
                                                    ORDER BY Application_Status__c, Effective_Date__c];
            
            for( Application__c app: applicationList ) {
                wrapper.add( new ApplicationWrapper(app) );
            }
            return wrapper;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class ApplicationWrapper {
        @AuraEnabled public String id;
        @AuraEnabled public String status;
        @AuraEnabled public String name; 
        @AuraEnabled public Date effectiveDate;
        @AuraEnabled public Integer sortOrder;
        
        ApplicationWrapper(Application__c app ) {
            this.id = app.Id;
            this.status = app.Application_Status__c;
            this.name = app.Name;
            this.effectiveDate = app.Effective_Date__c;
            
            if(this.status == TH_Constants.ACTIVE) {
                sortOrderActive += 1;
                this.sortOrder = sortOrderActive;
            } else if (this.status == TH_Constants.RENEWED){
                sortOrderRenewed += 1;
                this.sortOrder = sortOrderRenewed;
            } else if(this.status == TH_Constants.NEW) {
                sortOrderNew += 1;
                this.sortOrder = sortOrderNew;
            } else if(this.status == TH_Constants.DRAFT) {
                sortOrderDraft += 1;
                this.sortOrder = sortOrderDraft;
            } else if(this.status == TH_Constants.IN_PROGRESS) {
                sortOrderInProgress += 1;
                this.sortOrder = sortOrderInProgress;
            }  
        }
    }
    
}
